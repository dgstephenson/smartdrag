/* global io */

const client = (()=>{

  const paper = Snap(window.innerWidth,window.innerHeight)
  const group = paper.group()

  const socket = io({transports: ['websocket'], upgrade: false})
  const templates = {} 
  const bits = []
  const handlers = {}

  let panning = false
  let seed = null
  let restartNeeded = false 

  const unique = arr => {
    const u = {}
    return arr.filter(v => u[v] = (v !== undefined && !u.hasOwnProperty(v)) )
  }

  // Disable Right Click Menu
  document.oncontextmenu = () => false

  // Setup Zoom-Pan-Drag
  paperError = (error,paper) => console.log(error,paper)
  paper.zpd({zoom:true,pan:false,drag:false},paperError)
  paper.zoomTo(0.25,0)

  paper.mousedown(event => { 
    if(event.button==2) paper.zpd({pan:true},paperError) 
  })

  paper.mouseup(event => {
    if(event.button==2) paper.zpd({pan:false},paperError)
  })

  const addFragment = (fragment,x,y,rotation) => {
    const svg = fragment.select("g")
    paper.append(svg)
    const children = paper.children()
    const component = children[children.length-1]
    const width = component.getBBox().width
    const height = component.getBBox().height
    const startX = x - 0.5*width
    const startY = y - 0.5*height
    const startMatrix = component.transform().localMatrix.translate(startX,startY)
    startMatrix.rotate(rotation,width/2,height/2)
    component.transform(startMatrix)
    group.add(component)
    return component
  }

  const addComponent = (description) => {
    const {x,y,rotation,type,clones,file} = description
    const template = templates[file]
    const startMatrix = template.transform().localMatrix.translate(x,y)
    for(i=0; i<=clones; i++) {
      const component = template.clone()
      group.add(component)
      component.node.style.display = "block"
      component.transform(startMatrix)
      component.transform(`${component.transform().local}r${rotation}`)
      if(type==="bit") {
        bits.push(component)
        component.smartdrag()
        component.data('id',bits.length-1)
        component.data('file',file)
      }
    }
  }

  const setupTemplate = (file,descriptions,updates,numTemplates) => fragment => {
    const template = addFragment(fragment,0,0,0)
    template.node.style.display = "none"
    templates[file] = template
    if(Object.keys(templates).length===numTemplates) {
      descriptions.map(description=>addComponent(description))
      updates.map(processUpdate)
      setInterval(updateServer,400)
    }
  }

  const start = (descriptions,updates) => {
    const files = unique(descriptions.map(item=>item.file))
    files.map(file =>
      Snap.load(`assets/${file}.svg`,setupTemplate(file,descriptions,updates,files.length))
    )
  } 

  const describe = options => {
    const description = {file:null,x:0,y:0,type:"bit",clones:0,rotation:0}
    return Object.assign(description,options)
  }

  const on = (name,handler) => (handlers[name] = handler) 
  
  const updateServer = () => {
    const msg = { updates: [] }
    bits.map(bit => {
      if(bit.data('moved')) {
        const bitUpdate = {
          id: bit.data('id'),
          local: bit.transform().local 
        }
        if(handlers.moved) {
          Object.assign(bitUpdate,handlers.moved(bit))
        }
        msg.updates.push(bitUpdate)
        bit.data('moved',false)
      }
    })
    socket.emit('updateServer',msg)
  }

  const processUpdate = update => {
    const bit = bits[update.id]
    bit.stop()
    bit.animate({transform: update.local},400)
    if(handlers.update) handlers.update(update) 
  }

  socket.on('connect', () => {

    console.log('sessionid =',socket.id)

    socket.on('updateClient', msg => {
      if(msg.seed===seed) msg.updates.map(processUpdate)
    })

    socket.on('setup', msg => {
      if(!seed) {
        seed = msg.seed
        Math.seedrandom(seed)
        setup(msg)
      } else {
        console.log("Restart Needed")
      }
    })

  })

  return {describe, start, bits, on}

})()
